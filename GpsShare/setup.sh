#!/bin/sh
echo '**************************************************'
echo 'setup by ansible'
echo '**************************************************'
apt-get install -y ansible git
echo ==== download playbook ====
mkdir /home/vagrant/setup
git clone https://github.com/canpok1/ansible.git /home/vagrant/setup
echo ==== copy playbook ====
cp -rf /vagrant/Ansible/* /home/vagrant/setup/
chown -R vagrant:vagrant /home/vagrant/setup
echo ==== ansible run ====
ansible-playbook /home/vagrant/setup/setup.yml -i /home/vagrant/setup/localhost -vvv
echo ==== set JAVA_HOME ====
echo export JAVA_HOME=$(readlink -e `which javac` | sed "s:/bin/javac::") >> /home/vagrant/.bashrc
echo export PATH=\$JAVA_HOME/bin:\$PATH >> /home/vagrant/.bashrc
chown vagrant:vagrant /home/vagrant/.bashrc
echo ==== install nodejs ====
sudo apt-get -y install curl
sudo curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get -y install nodejs build-essential
