require 'spec_helper'

describe iptables do
  it { should have_rule ('-A INPUT -p tcp -m tcp --dport 80 -j ACCEPT') }
end
