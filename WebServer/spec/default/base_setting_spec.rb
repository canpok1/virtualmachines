require 'spec_helper'

describe command('date') do
  its(:stdout) { should match /JST/ }
end

describe file('/etc/sysconfig/i18n') do
  it { should be_file }
    its(:content) { should match /LANG="ja_JP.UTF-8/ }
end
