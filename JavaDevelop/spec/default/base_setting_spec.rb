require 'spec_helper'

describe command('date') do
  its(:stdout) { should match /JST/ }
end

describe command('which git') do
  its(:exit_status) { should eq 0 }
end

describe command('which ansible') do
  its(:exit_status) { should eq 0 }
end

