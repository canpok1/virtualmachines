require 'spec_helper'

describe command('which psql') do
  its(:exit_status) { should eq 0 }
end

describe service('postgresql') do
  it { should be_enabled }
  it { should be_running }
end

describe port('5432') do
  it { should be_listening }
end
