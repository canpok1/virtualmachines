#!/bin/sh
echo ==== set timezone ====
timedatectl set-timezone Asia/Tokyo

echo '**************************************************'
echo 'setup by ansible'
echo '**************************************************'
apt-get install -y software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update -y
apt-get install -y ansible git
echo ==== download playbook ====
mkdir -p /home/vagrant/setup/roles
git clone https://github.com/malk/ansible-java8-oracle.git /home/vagrant/setup/roles/ansible-java8-oracle
echo ==== copy playbook ====
cp -rf /vagrant/Ansible/* /home/vagrant/setup/
chown -R vagrant:vagrant /home/vagrant/setup
echo ==== ansible run ====
ansible-playbook /home/vagrant/setup/setup.yml -i /home/vagrant/setup/localhost -vvv

echo '**************************************************'
echo 'setup tools by command'
echo '**************************************************'
mkdir /home/vagrant/tools

echo ==== download tomcat ====
wget http://ftp.kddilabs.jp/infosystems/apache/tomcat/tomcat-8/v8.5.4/bin/apache-tomcat-8.5.4.tar.gz -P /home/vagrant/tools/
tar xvf /home/vagrant/tools/apache-tomcat-8.5.4.tar.gz -C /home/vagrant/tools
chown -R vagrant:vagrant /home/vagrant/tools

echo ==== install postgreSQL ====
echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" >> pgdg.list
sudo mv ./pgdg.list /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
  sudo apt-key add -
sudo apt-get update
sudo apt-get install -y postgresql-9.4 pgadmin3 postgresql-9.4-postgis-2.2
# change trust
sudo cp /etc/postgresql/9.4/main/pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf.back
sudo cp /vagrant/postgresql/all_trust_pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
sudo /etc/init.d/postgresql restart
# set postgres password
psql -U postgres -c "ALTER Role postgres PASSWORD 'postgres';"
sudo cp /vagrant/postgresql/pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf
sudo /etc/init.d/postgresql restart

echo ==== install vim ====
apt-get install -y vim

echo ==== download eclipse-installer ====
wget http://eclipse.stu.edu.tw/oomph/epp/neon/R/eclipse-inst-linux64.tar.gz -P /home/vagrant/tools/
wget http://iij.dl.osdn.jp/mergedoc/66003/pleiades_1.7.0.zip -P /home/vagrant/tools/
tar xavf /home/vagrant/tools/eclipse-inst-linux64.tar.gz
mkdir -p /home/vagrant/tools/eclipse_pleiades
cd /home/vagrant/tools/eclipse_pleiades
unzip /home/vagrant/tools/pleiades_1.7.0.zip
chown -R vagrant:vagrant /home/vagrant/tools
